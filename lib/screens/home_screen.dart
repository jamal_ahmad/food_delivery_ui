import 'package:flutter/material.dart';
import 'package:food_delivery/data/data.dart';
class HomeScreen extends StatefulWidget {
  const HomeScreen({Key key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        //leading: IconButton(icon: Icons.account_circle, onPressed: onPressed),
        leading: IconButton(
          icon: Icon(Icons.account_circle),
          iconSize: 30.0,
          onPressed: () {},
        ),
        title: Text('Food Delivery'),
        centerTitle: true,
        actions: [
          TextButton(
              onPressed: () {},
              child: Text('Cart (${currentUser.cart.length})'))
        ],
      ),
    );
  }
}
